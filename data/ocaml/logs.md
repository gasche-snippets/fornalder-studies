## Preparation of the package database

At the time of writing, opam-repository has master commit 01c350d759f8d4e3202596371818e6d997fa5fe2.

```sh
mkdir -p data/ocaml

$ time git clone --depth 1 https://github.com/ocaml/opam-repository.git data/ocaml/opam-repo

# all homepages
$ find data/ocaml/opam-repo/ -name opam | xargs grep -Eh '^ *homepage:' | sort -u | wc -l
2658

# all dev-repo
$ find data/ocaml/opam-repo/ -name opam | xargs grep -Eh '^ *dev-repo:' | sort -u | wc -l
2569
# so: it's fine to just look at dev-repos

# all git dev-repos
$ find data/ocaml/opam-repo/ -name opam | xargs grep -Eh '^ *dev-repo:' | sort -u | grep git | wc -l
2525
# we lose some repos, but not very much

# git dev-repos with git+https URLs
$ find data/ocaml/opam-repo/ -name opam | xargs grep -Eh '^ *dev-repo:' | sort -u | grep -v git+https | wc -l
664
# there is a sizeable number of repositories not using the git+https scheme

# store all git dev-repos in a file
$ find data/ocaml/opam-repo/ -name opam | xargs grep -Eh '^ *dev-repo:' | sort -u | grep git \
  | sed 's/dev-repo://g' | sed 's/"//g' > data/ocaml/git-repos.txt
$ wc -l git-repos.txt
2525 data/ocaml/git-repos.txt
```


## Turning git repo URLs into git clone command

```sh
# the file obtained from the opam-repository
$ wc -l data/ocaml/git-repos.txt
2525 git-repos.txt

# creating the git command file
$ sh scripts/fix-opam-repo-urls.sh < data/ocaml/git-repos.txt | sort -u > data/ocaml/git-urls.txt

$ wc -l data/ocaml/git-urls.txt
2463 data/ocaml/git-urls.txt
# some URLs are duplicates
```

Some repositories are not actually OCaml projects, but upstream
projects referenced for packaging. We filter them out.

```sh
$ sh scripts/fix-opam-repo-urls.sh < data/ocaml/git-repos.txt | sort -u \
  | grep -v "github.com/mysql/mysql-server.git" \
  | grep -v "github.com/Z3prover/z3.git" \
  | grep -v "llvm.org/git/llvm.git" \
  | grep -v "github.com/systemd/systemd" \
   > data/ocaml/git-urls.txt
$ wc -l data/ocaml/git-urls.txt
2459 data/ocaml/git-urls.txt
```

## Cloning all git repos

I use the 'GNU parallel' command to get a progress bar.

```sh
$ mkdir -p data/ocaml/tmp
$ mkdir -p data/ocaml/repos

$ time parallel -j100 --bar 'sh scripts/clone.sh data/ocaml/tmp data/ocaml/repos ' < data/ocaml/git-urls.txt
16m60s

# note: in practice a couple repositories take forever to download,
# so I killed the process with 10 repos remaining

# total download count and size
$ ls -1 data/ocaml/repos | wc -l
2362
$ du -sch data/ocaml/repos
2.2G
```



## Ingesting data

```sh
$ git clone https://github.com/hpjansson/fornalder.git

$ (cd fornalder; cargo build)

# ingest data for github/ocaml/ocaml
$ time fornalder/target/debug/fornalder ingest data/ocaml/ocaml.db data/ocaml/repos/ocaml---*
5s

# ingest data for all repositories
$ time fornalder/target/debug/fornalder ingest data/ocaml/opam-repo.db data/ocaml/repos/*
8m
```

Note: you can now get rid of the cloned repositories to save disk
space. However, keeping them around can be useful if you find out that
you want to tweak the data collection (filter out some bad repos,
etc.) and re-run ingestion.

```sh
$ du -sch data/ocaml/repos
2.1G
$ du -sch data/ocaml/opam-repo.db
205M
```

## Plotting graphs

```sh
$ time sh scripts/plot.sh data/ocaml/ocaml.db
1s
$ time sh scripts/plot.sh data/ocaml/opam-repo.db
16s

$ <your favorite viewer> data/ocaml/*.png
```


