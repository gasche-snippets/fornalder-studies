# Get all OCaml packages on npm

Note: for npm registry queries, see
https://stackoverflow.com/questions/34071621/query-npmjs-registry-via-api

```
# get raw data: packages with keyword 'ocaml'
wget "http://registry.npmjs.com/-/v1/search?text=keywords:ocaml&size=1000" -O data/npm-ocaml/packages.json

# number of downloaded packages
$ jq ".objects|length" data/npm-ocaml/packages.json
183

# number of downloaded packages with "repositories" metadata
$ jq "[.objects[].package.links.repository | values] | length" data/npm-ocaml/packages.json
134
 
# store the available repositories;
# we filter out esy-ocaml/ocaml:  duplicates many of the github:/ocaml/ocaml commits
# with different hashes, creating a fair amount of noise
jq ".objects[].package.links.repository | values" data/npm-ocaml/packages.json \
  | sed 's,",,g' \
  | grep -v https://github.com/esy-ocaml/ocaml \
  > data/npm-ocaml/git-urls.txt
```



# Clone all repositories

I use the 'parallel' command to get a nice progress bar.

```sh
$ mkdir -p data/npm-ocaml/tmp
$ mkdir -p data/npm-ocaml/repos

$ time parallel -j100 --bar 'sh scripts/clone.sh data/npm-ocaml/tmp data/npm-ocaml/repos ' \
    < data/npm-ocaml/git-urls.txt
0m14s

# note: there are many failures due to different URLs trying to write repositories of the same name;
# I think that the clashes come from the same project having migrated forges between versions.
# We should have filtered to work only with the latest version of each directory.

# total download count and size
$ ls -1 data/npm-ocaml/repos | wc -l
105
```


## Ingesting data

```
$ git clone https://github.com/hpjansson/fornalder.git

$ (cd fornalder; cargo build)

$ time target/debug/fornalder ingest data/npm-ocaml/npm-ocaml.db data/npm-ocaml/repos/*
6s
```

## Plotting graphs

```sh
$ time sh scripts/plot.sh data/npm-ocaml/npm-ocaml.db
1.4s

$ <your favorite viewer> data/ocaml/*.png
```
