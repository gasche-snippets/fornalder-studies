## Preparation of the package database

At the time of writing, opam-repository has master commit 07e970a91edc0d9e424d33c2659e27c791515fc6

```sh
mkdir -p data/coq

$ time git clone --depth 1 https://github.com/coq/opam-coq-archive.git data/coq/opam-repo
1s

# all homepages
$ find data/coq/opam-repo/ -name opam | xargs grep -Eh '^ *homepage:' | sort -u | wc -l
404

# all dev-repo
$ find data/coq/opam-repo/ -name opam | xargs grep -Eh '^ *dev-repo:' | sort -u | wc -l
374
# we loose a bit, but not too much

# all git dev-repos
$ find data/coq/opam-repo/ -name opam | xargs grep -Eh '^ *dev-repo:' | sort -u | grep git | wc -l
373
# everyone uses git

# store all git dev-repos in a file
$ find data/coq/opam-repo/ -name opam | xargs grep -Eh '^ *dev-repo:' | sort -u | grep git \
  | sed 's/dev-repo://g' | sed 's/"//g' > data/coq/git-repos.txt
$ wc -l data/coq/git-repos.txt
375 git-repos.txt
```


## Turning git repo URLs into git clone command

```sh
# the file obtained from the opam-repository
$ wc -l data/coq/git-repos.txt
375 git-repos.txt

# creating the git command file
$ sh scripts/fix-opam-repo-urls.sh < data/coq/git-repos.txt | sort -u > data/coq/git-urls.txt
$ wc -l data/coq/git-urls.txt
375 git-urls.txt
```

Some repositories are not actually Coq projects, but upstream projects
referenced for packaging. We filter them out.

```sh
$ sh scripts/fix-opam-repo-urls.sh < data/coq/git-repos.txt | sort -u \
  | grep -v "gitlab.inria.fr/fpottier/menhir.git" \
  > data/coq/git-urls.txt

$ wc -l data/coq/git-urls.txt
374 git-urls.txt
```


## Cloning all git repos

I use the 'parallel' command to get a nice progress bar.

```sh
$ mkdir -p data/coq/tmp
$ mkdir -p data/coq/repos

$ time parallel -j100 --bar 'sh scripts/clone.sh data/coq/tmp data/coq/repos ' < data/coq/git-urls.txt
3m

# Note: 'robot.git' and 'tlc.git' take forever to clone, so I killed
# the download and they are not included in the results.

# total download count and size
$ ls -1 data/coq/repos | wc -l
355
$ du -sch data/coq/repos
243M
```

## Ingesting data

```sh
$ git clone https://github.com/hpjansson/fornalder.git

$ (cd fornalder; cargo build)

# ingest data for github/coq/coq
$ time fornalder/target/debug/fornalder ingest data/coq/coq.db data/coq/repos/coq.git---*
12s

# ingest data for all repositories
$ time fornalder/target/debug/fornalder ingest data/coq/opam-repo.db data/coq/repos/*
3m
```

Note: you can now get rid of the cloned repositories to save disk
space. However, keeping them around can be useful if you find out that
you want to tweak the data collection (filter out some bad repos,
etc.) and re-run ingestion.

```sh
$ du -sch data/coq/repos
243M
$ du -sch data/coq/opam-repo.db
34M
```


## Plotting graphs

```sh
$ time sh scripts/plot.sh data/coq/coq.db
1s
$ time sh scripts/plot.sh data/coq/opam-repo.db
5s

$ <your favorite viewer> data/coq/*.png
```


