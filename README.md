# Fornalder scripts and data

This repository contains my [scripts](scripts/) used to analyze sets
of git repositories using Hans Petter Jansson's
[fornalder](https://github.com/hpjansson/fornalder) tool, with my
working logs, data and the generated visualizations for the
repositories I studied.

I'm sharing this in the hope of making it easier for other people to
analyze their own software communities of interest. Each directory
contains a `logs` file that is basically a step-by-step guide to do
your own, although of course you will have to find out for yourself
which repositories to clone and how to curate this data.


## Studies

- [data/ocaml](data/ocaml): OCaml repositories (just the compiler, or
  all projects available on opam).

  See the [Analysis results](data/ocaml/README.md).

- [data/coq](data/coq): Coq repositories (just the proof assistant,
  or all projects available on opam).

  See the [Analysis results](data/coq/README.md).

- [data/npm-ocaml](data/npm-ocaml) projects packaged on npm with the tag "ocaml",
  which typically correspond to OCaml software mainly maintained using 'esy'
  and distributed as Javascript artifacts.


## Other projects/communities studied using fornalder

- Gnome: https://hpjansson.org/blag/2020/12/16/on-the-graying-of-gnome/
- KDE: https://carlschwan.eu/2021/04/29/health-of-the-kde-community/ )


## Interested in running your own analysis?

Please feel free to run your own analysis on your community of
interest. (You can send a MR here if you wish to, or just have your
own place for your data; let me know if you want it listed above.)

It should be relatively easy to get started by just cloning this
repository, and then reproducing the steps in one of the existing
`data/` repositories -- they are listed and explained in their
respective `logs.md` file, see for example
[data/ocaml/logs.md](data/ocaml/logs.md).

The part that you will have to figure out on your own is the set of
git repos you want to clone. (If your community has a standard base of
repositories, this is rather easy, but otherwise you have to create
a list manually.)


## Fornalder improvements

In the process of doing this work, I submitted a few improvements or
feature requests to the upstream fornalder codebase. I had a very
pleasant experience, the maintainer
([hpjannsson](https://github.com/hpjansson)) being very reactive and
willing to write new code -- which I find impressive for a one-off
script they may not reuse in the future.

More specifically:

- I improved documentations for some aspects of the tool that I had to
  find out by myself (how to inspect the generated database
  ([#9](https://github.com/hpjansson/fornalder/pull/9)), what git
  cloning options are recommended
  ([#14](https://github.com/hpjansson/fornalder/pull/14))).

- I fixed the tool to correctly use the project `.mailmap` files,
  which was necessary to correctly analyze the Coq
  codebase. ([#10](https://github.com/hpjansson/fornalder/pull/10))

- On my suggestion, hpjannsson added support for the `--filter=blob:none`
  cloning mode (only commit metadata, no repository data), which
  massively speeds up the analysis. (From 30m to 8m for all OCaml software).
  ([#13](https://github.com/hpjansson/fornalder/issues/13)).
