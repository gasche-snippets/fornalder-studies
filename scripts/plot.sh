# This script generates two consistently-named plots from a fornalder-ingested database foo.db
#   foo-contributors.png: plots the number of contributors/authors per yearly cohort
#   foo-commits.png:      plots the volume of commits per yearly cohort
# It assumes that a 'fornalder' subdirectory exists and has been built

# Example: sh scripts/plot.sh data/coq/opam-repo.db
DB=$1
NAME=$(echo $DB | sed -E 's,.db$,,')
fornalder/target/debug/fornalder plot $DB --unit authors $NAME-contributors.png
fornalder/target/debug/fornalder plot $DB --unit commits $NAME-commits.png
