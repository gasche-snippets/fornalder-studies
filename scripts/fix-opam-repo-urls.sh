# This script "fixes" URLs typically found in the dev-repo metadata of
# opam repositories to turn them into git-clonable URLs.

# Example: sh scripts/fix-git-clone-urls.sh < data/ocaml/git-repos | sort -u > data/ocaml/git-urls.txt

while read url
do
  case $url in
    git+http://*|git+https://*|git+ssh://*)
      # remove the 'git+' part at the beginning
      echo $url | sed 's,^git+,,'
      ;;
    git+ttps://*)
      # one URL in opam-repository has this typo
      echo $url | sed "s,^git+ttps,https,"
      ;;
    git://*)
      # some projects use git://http://, drop the first part
      # turn git://foo into http://foo
      # turn http://github.com:username/repo into http://github.com/username/repo
      echo $url | sed "s,^git://http://,http://,g" | sed "s,^git,http,g" | sed "s,github.com:,github.com/,g"
      ;;
    *)
      echo UNKNOWN $url
      exit 1
  esac
done
