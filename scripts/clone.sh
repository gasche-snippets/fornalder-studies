# This script is in charge of doing a partial clone of a git
# repository for analysis by fornalder. It can safely be run on many
# repositories in parallel.

# Example: sh scripts/clone.sh data/ocaml/tmp data/ocaml/repos http://github.com/domsj/ocaml-quickcheck
TMP_DIR=$1
FINAL_DIR=$2
URL=$3

NAME=$(echo $URL | sed -E 's,.*/,,g')--url$(echo $URL | shasum | cut -b 1-8)
# we include a (partial) hash of the download URL so that repositories
# with the same name at different addresses do not conflict
# (note: the data ingestion tool will correctly deduplicate same-hash commits)

GIT_CLONE_OPTIONS='--bare --filter=blob:none'
# these options reduce the amount of downloaded data
# note: --filter=blob:none is unsupported by some git servers (they will warn and ignore the option),
#  and disables the inspection of the number of changed lines
#  (in our experience the changed-line metric gives noisy data that is unusable anyway)

# we are careful to clone the repo in a temporary directory first, which has two benefits:
# 1. if we have to stop the clone operation in flight, we don't end up
#    with an invalid repository; you can just erase tmp/
#    and restart without losing the succesful clones
# 2. if there is a long trail of repositories taking very long to clone, it suffices to check
#    inside the temporary directory to know which repositories are at fault.

# do not try to re-clone a repository that succeeded in the past;
# this makes running the tool several time skip the already-downloaded projects as expected
if [ -d $FINAL_DIR/$NAME ]; then exit 0; fi
git clone --quiet $GIT_CLONE_OPTIONS $URL $TMP_DIR/$NAME || exit 1
mv $TMP_DIR/$NAME $FINAL_DIR/$NAME
